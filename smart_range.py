from typing import Iterable


def smart_range(start: int, end: int, step: int = 1, end_inclusive: bool = True) -> Iterable[int]:
    if step <= 0:
        raise ValueError("step must be positive (if you meant a descending smart range, use start > end)")
    value = start
    if start < end:
        while value < end:
            yield value
            value += step
    else:
        while value > end:
            yield value
            value -= step
    if end_inclusive:
        yield value


def main():
    for i in smart_range(0, 10):
        print(i)


if __name__ == '__main__':
    main()
from datetime import datetime
from typing import List

from smart_range import smart_range


class Participation:

    def __init__(self, participant_name: str, score: int):
        self.participant_name = participant_name
        self.score = score
        self.score_achieved = datetime.now()


class Tournament:

    def __init__(self, rounds: int, participant_names: List[str]):
        self.rounds = rounds
        self.state = [Participation(name, 0) for name in participant_names]

    def run(self):
        for current_round in smart_range(1, self.rounds):
            print("Round: {} of {}".format(current_round, self.rounds))
            participant = self.ask_who_won()
            self.update_state(round_winner=participant)
            self.present_state(current_round)
        print("Tournament concluded! {} wins!".format(self.state[0].participant_name))

    def ask_who_won(self) -> str:
        winner_name = input("Who won? ")
        while winner_name not in [participation.participant_name for participation in self.state]:
            winner_name = input("Participant {} not found. Who won? ".format(winner_name))
        return winner_name

    def update_state(self, round_winner: str):
        for participation in self.state:
            if participation.participant_name == round_winner:
                participation.score += 1
                participation.score_achieved = datetime.now()
                break
        now = datetime.now()
        self.state.sort(key=lambda p: (p.score, now - p.score_achieved), reverse=True)

    def present_state(self, current_round: int):
        print("Round {} concluded.".format(current_round))
        for i in smart_range(0, len(self.state), end_inclusive=False):
            score = self.state[i].score
            participant_name = self.state[i].participant_name
            if score == 1:
                point_s = "point"
            else:
                point_s = "points"
            print("{}.\t{} {} {}".format(i + 1, participant_name.ljust(15), score, point_s))
        print()


def main():
    print("Hello, let's have a tournament! :)")
    rounds = int(input("How many rounds should the tournament have? "))
    names = input("Please enter participants' names, separated with commas: ")  # some test data:
    # Michał, Piotrek, Agata, Maria, Tomek, Władysław, Dorota

    Tournament(rounds=rounds, participant_names=names.replace(", ", ",").split(",")).run()


if __name__ == '__main__':
    main()

# some future todos:
# configuring which place gives how many points per round (places per round first) (now it's win = 1, else = 0 points)
# configuring ex-aequo (now there is none, when multiple players have the same score, first to achieve the score wins)
# doing something about the number of rounds not being an integer, instead of just crashing
# smarter, adaptive padding (participant name .......... points), based on the longest name
# maybe participant name case-insensitive when asking who won?
# suggesting participant name while typing who won, or better, how docker does it (first n letters until it's unique)
# saving the state to a file, to resume playing later?

class Question:

    def __init__(self, text, answers, correct_answer_key):
        self.text = text
        self.answers = answers
        self.correct_answer_key = correct_answer_key

    def is_answer_correct(self, key):
        return key == self.correct_answer_key

    def prompt(self):
        answers = ""
        for answer in self.answers:
            answers += answer.key + ": " + answer.text + "\n"
        return self.text + "\n" + answers + "\n"

    # checks if the key is - not necessarily correct - but at least not like E,
    # where the possible answers are only A, B, C and D
    def is_answer_valid(self, key):
        for answer in self.answers:
            if answer.key == key:
                return True
        return False


class Answer:

    def __init__(self, key, text):
        self.key = key
        self.text = text


questions = [
    Question("Flag of Poland (from the top)?", [
        Answer("a", "red-blue-white"),
        Answer("b", "blue-white"),
        Answer("c", "red-white"),
        Answer("d", "white-red")
    ], correct_answer_key="d"),
    Question("What color is a tomato?", [
        Answer("a", "green"),
        Answer("b", "red"),
        Answer("c", "brown")
    ], correct_answer_key="b"),
    Question("Is Earth a planet?", [
        Answer("a", "yes"),
        Answer("b", "no")
    ], correct_answer_key="a")
]

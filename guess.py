from random import randint

print("Hello! This is a guessing game.")
print("The game will now randomly choose an integer between 0 and 100, inclusive.")
print("Guess the number! :)")

the_number = randint(0, 100)
guess = int(input("Input the number: "))
while guess != the_number:
    if guess > the_number:
        tip = "less"
    else:
        tip = "more"
    guess = int(input(f"Nope, try again! (Tip: {tip}): "))

print("Hey, you guessed it! :)")

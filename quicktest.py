import unittest
from quicksort import quicksort

test_lists = [
    [1, 2, 3, 4, 5, 6, 7, 8, 9],
    [9, 8, 7, 6, 5, 4, 3, 2, 1],
    [2, 2],
    [3, 3, 3],
    [4, 4, 4, 4],
    [1, 2],
    [2, 1],
    [2],
    [],
    [7, 8, 4],
    [3, 8, 6, 4],
    [5, 3, 2, 7, 1],
    [0, 4, 7, 3, 6, 2],
    [7, 1, 2, 5, 2, 7, 3],
    [4, 5, 2, 9, 0, 1, 4, 6],
    [7, 4, 3, 6, 1, 9, 0, 3, 7],
    [4, 5, 6, 2, 3, 1, 5, 7, 7, 3],
    [0, 0, 0, 3, 2, 7, 2, 5, 3, 7, 8]
]


class QuicksortTestCase(unittest.TestCase):

    def test_it_should_sort_correctly(self):

        # given: each list from the above test params
        for test_list in test_lists:
            with self.subTest():

                # when: we sort the list
                print("before = " + str(test_list))
                quicksort(test_list)
                print("after = " + str(test_list))
                print()

                # then: the list is sorted
                i = 0
                while i < len(test_list) - 1:
                    # ... which means, that in any given pair of consecutive numbers
                    # the one on the right is greater or equal to the one on the left
                    self.assertLessEqual(test_list[i], test_list[i + 1])
                    i += 1


if __name__ == '__main__':
    unittest.main()

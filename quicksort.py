def quicksort(elements):
    _actual_quicksort(elements, 0, len(elements) - 1)


def _actual_quicksort(elements, first_index, last_index):
    if last_index - first_index < 1:
        return
    checkpoint = elements[(first_index + last_index) // 2]
    left, right = first_index, last_index
    while left <= right:
        while elements[left] < checkpoint:
            left += 1
        while elements[right] > checkpoint:
            right -= 1
        if left < right:
            _swap(elements, left, right)
            left += 1
            right -= 1
        else:
            break
    _actual_quicksort(elements, first_index, right)
    _actual_quicksort(elements, right + 1, last_index)


def _swap(elements, i, j):
    temp = elements[i]
    elements[i] = elements[j]
    elements[j] = temp

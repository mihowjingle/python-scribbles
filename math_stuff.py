from math import sqrt
from typing import Tuple, Optional


class LinearEquation:

    def __init__(self, a: float, b: float) -> None:
        self.a = a
        self.b = b

    def calculate(self, x: float) -> float:
        return self.a * x + self.b

    def zero_point(self) -> Optional[float]:
        if self.a == 0:
            return None
        return -self.b / self.a


class QuadraticEquation:

    def __init__(self, a: float, b: float, c: float) -> None:
        self.a = a
        self.b = b
        self.c = c

    def calculate(self, x: float) -> float:
        return self.a * x ** 2 + self.b * x + self.c

    @property
    def delta(self) -> float:
        return self.b ** 2 - 4 * self.a * self.c

    def zero_points(self) -> Optional[Tuple[float, float]]:
        delta = self.delta
        if delta < 0:
            return None
        x01 = (-self.b - sqrt(delta)) / (2 * self.a)
        x02 = (-self.b + sqrt(delta)) / (2 * self.a)
        return x01, x02


def factorial(a: int) -> int:
    if a != int(a) or a < 0:
        raise ValueError("Factorial accepts non-negative integers!")
    if a == 0:
        return 1
    return factorial(a - 1) * a


print("5! =", factorial(5))

def get_iterations() -> int:
    print("How precise do you want \u03c0 to be?")
    it = input()
    return int(it)


# Wallis

def calculate_part(i):
    up = 2 * i
    down_left = up - 1
    down_right = up + 1
    return up ** 2 / (down_left * down_right)


iterations = get_iterations()
half_pi = 1
for x in range(1, iterations + 1):
    half_pi *= calculate_part(x)
    print("{} iterations: {}".format(x, 2 * half_pi))

# or... Gregory-Leibniz

iterations = get_iterations()
quarter_pi = 1
divisor = 3
iteration = 1
while iteration <= iterations:
    quarter_pi -= 1 / divisor
    quarter_pi += 1 / (divisor + 2)
    divisor += 4
    print("{} iterations: {}".format(iteration, 4 * quarter_pi))
    iteration += 1

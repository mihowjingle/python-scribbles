from quiz_data import questions

print("Hello, let's take a quiz! :)\n")

answered_correctly = 0

for question in questions:
    key = input(question.prompt())
    print()
    if question.is_answer_correct(key):
        print("Congratulations! That's right :)\n")
        answered_correctly += 1
    elif question.is_answer_valid(key):
        print("Wrong... :/\n")
    else:
        print("That wasn't even a possible answer. You little trollo-lollo :P\n")

print()

total = len(questions)
print(f"{answered_correctly} of {total} questions answered correctly!")
if answered_correctly == len(questions):
    print("Are you some kind of genius? :P")
